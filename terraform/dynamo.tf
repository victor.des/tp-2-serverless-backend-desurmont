# resource aws_dynamodb_table job-table

resource "aws_dynamodb_table" "job_table" {
  name           = var.dynamo_job_table_name
  billing_mode   = "PROVISIONED"
  read_capacity  = 5
  write_capacity = 5

  hash_key = "id"
  range_key = "city"

  attribute {
    name = "id"
    type = "S"  // 'S' stands for String, which is appropriate for an 'id'
  }

  attribute {
    name = "city"
    type = "S"  // 'S' stands for String, which is appropriate for a 'city'
  }

  // ... You can add more settings like global secondary indexes or local secondary indexes if needed.
}