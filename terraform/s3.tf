# resource aws_s3_bucket s3_job_offer_bucket
resource aws_s3_bucket "s3_job_offer_bucket_desurmont_benmussa_lozac"{
  bucket = "s3-job-offer-bucket-bucket-desurmont-benmussa-lozac"
}


# resource aws_s3_object job_offers
resource aws_s3_object "job_offers"{
  bucket = aws_s3_bucket.s3_job_offer_bucket_desurmont_benmussa_lozac.id
  key = "job_offers/"
}



# resource aws_s3_bucket_notification bucket_notification

resource "aws_s3_bucket_notification" "bucket_notification" {
  bucket = aws_s3_bucket.s3_job_offer_bucket_desurmont_benmussa_lozac.id

  lambda_function {
    lambda_function_arn = aws_lambda_function.s3_to_sqs_lambda.arn
    events             = ["s3:ObjectCreated:*"]
    filter_prefix      = "job_offers/"
    filter_suffix      = ".csv"
    }
    } 